AI Core API
===========

Lifecycle of AI scenarios.

.. module:: procaine.aicore


Connect to AI Core API
----------------------

.. autoclass:: Client

Scenarios & Templates
---------------------

.. automethod:: Client.list_scenarios
.. automethod:: Client.list_templates
.. automethod:: Client.template


Executions
----------

.. automethod:: Client.list_executions
.. automethod:: Client.create_execution
.. automethod:: Client.execution
.. automethod:: Client.execution_logs
.. automethod:: Client.stop_execution


Artifacts
---------

.. automethod:: Client.list_artifacts
.. automethod:: Client.create_artifact
.. automethod:: Client.artifact


Deployments
-----------

.. automethod:: Client.list_deployments
.. automethod:: Client.create_deployment
.. automethod:: Client.deployment
.. automethod:: Client.deployment_logs
.. automethod:: Client.stop_deployment
.. automethod:: Client.delete_deployment


Inference
---------

.. automethod:: Client.predict


Configuration and secrets
-------------------------

Resource Groups
~~~~~~~~~~~~~~~
.. automethod:: Client.list_resource_groups
.. automethod:: Client.create_resource_group

Git repositories
~~~~~~~~~~~~~~~~

.. automethod:: Client.list_git_repositories
.. automethod:: Client.create_git_repository
.. automethod:: Client.delete_git_repository

Sync applications
~~~~~~~~~~~~~~~~~

.. automethod:: Client.list_applications
.. automethod:: Client.application
.. automethod:: Client.create_application
.. automethod:: Client.delete_application

Docker registry secrets
~~~~~~~~~~~~~~~~~~~~~~~

.. automethod:: Client.list_docker_registry_secrets
.. automethod:: Client.create_docker_registry_secret
.. automethod:: Client.delete_docker_registry_secret

Object-store secrets
~~~~~~~~~~~~~~~~~~~~

.. automethod:: Client.list_s3_secrets
.. automethod:: Client.create_s3_secret
.. automethod:: Client.delete_s3_secret

Generic secrets
~~~~~~~~~~~~~~~

.. automethod:: Client.list_generic_secrets
.. automethod:: Client.create_generic_secret
.. automethod:: Client.delete_generic_secret


Misc
----

.. automethod:: Client.meta
.. automethod:: Client.kpi
.. automethod:: Client.healthz
