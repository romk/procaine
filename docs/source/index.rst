Welcome to Procaine's documentation!
====================================

**Procaine** is a Python library for SAP AI Core.

It offers a simple interface to access AI Core REST API.


.. toctree::
   :maxdepth: 2

   install
   quickstart
   api
   examples
