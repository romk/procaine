Examples
========

More usage examples.


Input artifact
--------------

Upload a file to the registered S3 bucket *s3://my-bucket/my-path/my-object*.
The bucket `should be registered <https://procaine.readthedocs.io/en/latest/api.html#procaine.aicore.Client.create_s3_secret>`_.


Workflow Template
~~~~~~~~~~~~~~~~~

Submit the template to the git repo.  It uses an input artifact named ``test`` of kind *dataset*.
The artifact is mounted under ``/mnt/test``:

.. code-block:: yaml

  apiVersion: argoproj.io/v1alpha1
  kind: WorkflowTemplate
  metadata:
    name: artifact-example
    labels:
      scenarios.ai.sap.com/id: examples
      ai.sap.com/version: "0.1.0"
    annotations:
      scenarios.ai.sap.com/name: examples
      scenarios.ai.sap.com/description : "Examples"
      executables.ai.sap.com/name: artifact-example
      executables.ai.sap.com/description: "Artifacts example"
      sidecar.istio.io/inject: "false"
      artifacts.ai.sap.com/test.kind: dataset
  spec:
    entrypoint: start
    templates:
    - name: start
      inputs:
        artifacts:
        - name: test
          path: /mnt/test
      container:
        image: alpine:latest
        command: [ls]
        args: ["-l", "/mnt/test"]


Connect to AI Core
~~~~~~~~~~~~~~~~~~

Create a *Client* and check the template is synced::

  >>> from procaine.aicore import Client
  >>> from pprint import pprint

  >>> auth = {"url": AUTH_URL, "clientid": CLIENT_ID, "clientsecret": CLIENT_SECRET}
  >>> api = Client(AI_API_URL, auth)

  >>> example = api.template("artifact-example", scenario="examples")
  >>> pprint(example)
  {'createdAt': '2022-07-10T13:55:10+00:00',
   'deployable': False,
   'description': 'Artifacts example',
   'id': 'artifact-example',
   'inputArtifacts': [{'name': 'test'}],
   'modifiedAt': '2022-07-10T14:00:29+00:00',
   'name': 'artifact-example',
   'outputArtifacts': [],
   'parameters': [],
   'scenarioId': 'examples',
   'versionId': '0.1.0'}


Execute a workflow
~~~~~~~~~~~~~~~~~~

Run the flow after the template is synced, pass the object's S3 URL to bind it to the input ``test`` artifact::

  >>> obj = "s3://my-bucket/my-path/my-object"
  >>> api.create_execution(example, artifacts={"test": obj})
  {'id': 'e83b266f4c760b25', 'message': 'Execution scheduled', 'status': 'UNKNOWN', 'targetStatus': 'COMPLETED'}


View execution result
~~~~~~~~~~~~~~~~~~~~~

Check the reported artifact size is identical to the uploaded file to S3::

  >>> api.execution("e83b266f4c760b25")
  { ... 'status': 'COMPLETED', ... 'targetStatus': 'COMPLETED'}

  >>> logs = api.execution_logs("e83b266f4c760b25")
  >>> print(logs)
  time="2022-07-10T18:00:00.139Z" level=info msg="Starting Workflow Executor" executorType=docker version=v3.2.2
  time="2022-07-10T18:00:00.142Z" level=info msg="Creating a docker executor"
  ...
  time="2022-07-10T18:00:00.142Z" level=info msg="Start loading input artifacts..."
  time="2022-07-10T18:00:00.142Z" level=info msg="Downloading artifact: test"
  ...
  -rw-------    1 nobody   1337          1219 Jul 10 18:00 /mnt/test


Deploy a model
--------------

Use a docker image with a model inside.


Serving Template
~~~~~~~~~~~~~~~~

Submit a serving template to the registered git repo:

.. code-block:: yaml

  apiVersion: ai.sap.com/v1alpha1
  kind: ServingTemplate
  metadata:
    name: half-plus-two
    labels:
      scenarios.ai.sap.com/id: examples
      ai.sap.com/version: "0.1.0"
    annotations:
      scenarios.ai.sap.com/name: examples
      scenarios.ai.sap.com/description: Examples
      executables.ai.sap.com/name: half-plus-two
      executables.ai.sap.com/description: TensorFlow Serving example saved_model_half_plus_two_cpu
  spec:
    template:
      apiVersion: serving.kserve.io/v1beta1
      metadata:
	labels: |
	  ai.sap.com/resourcePlan: starter
      spec: |
	predictor:
	  minReplicas: 1
	  containers:
	  - name: kserve-container
	    image: romk/saved_model_half_plus_two_cpu
	    ports:
	      - containerPort: 8501
		protocol: TCP


Start a model server
~~~~~~~~~~~~~~~~~~~~

After `Connect to AI Core`_ and the serving template is synced, run::

  >>> model = api.create_deployment("half-plus-two")
  >>> model
  {'deploymentUrl': '', 'id': 'db48746908c0f53a', 'message': 'Deployment scheduled.', 'status': 'UNKNOWN'}


Wait for the deployment start
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Deployment should return the ``RUNNING`` status::

  >>> api.deployment(model)["status"]
  'PENDING'
  >>> api.deployment(model)["status"]
  'RUNNING'
  >>> print(api.deployment_logs(model))
  ...
  2022-10-22 20:00:57.523855: I tensorflow_serving/core/loader_harness.cc:87] Successfully loaded servable version {name: saved_model_half_plus_two_cpu version: 123}
  ...
  2022-10-22 20:00:57.530174: I tensorflow_serving/model_servers/server.cc:414] Exporting HTTP/REST API at:localhost:8501 ...
  [evhttp_server.cc : 245] NET_LOG: Entering the event loop ...


Inference the model
~~~~~~~~~~~~~~~~~~~

Get model status::

  >>> pprint(api.predict(model, "/v1/models/saved_model_half_plus_two_cpu"))
  {'model_version_status': [{'state': 'AVAILABLE',
                           'status': {'error_code': 'OK', 'error_message': ''},
                           'version': '123'}]}

Make a prediction request::

  >>> api.predict(model, "/v1/models/saved_model_half_plus_two_cpu:predict", {"instances": [1.0, 2.0]})
  {'predictions': [2.5, 3.0]}
