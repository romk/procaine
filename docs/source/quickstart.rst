Quickstart
==========

Run your first flow on AI Core.


Connect to AI Core
------------------

Follow `Provisioning SAP AI Core <https://help.sap.com/docs/AI_CORE/2d6c5984063c40a59eda62f4a9135bee/38c4599432d74c1d94e70f7c955a717d.html>`_ instructions.  After completion, there should be credentials to connect to AI Core::

  >>> from procaine.aicore import Client

  >>> auth = {"url": AUTH_URL, "clientid": CLIENT_ID, "clientsecret": CLIENT_SECRET}
  >>> api = Client(AI_API_URL, auth)

  >>> api.healthz()
  {'message': 'OK', 'status': 'READY'}


Register a git repo
-------------------

Register a git repo to where workflow templates would be stored::

  >>> api.create_git_repository("my-git", GIT_URL, USERNAME, PASSWORD)
  {'id': 'my-git', 'message': 'Repository has been on-boarded.'}


Sync the git repo with AI Core
------------------------------

Create an *application* to sync AI Core with the registered git repo::

  >>> api.create_application(repo_name="my-git")
  {'id': 'my-git', 'message': 'Application has been successfully created.'}

  >>> api.application("my-git")
  {'healthStatus': 'Healthy', ..., 'reconciledAt': '2022-10-22T19:09:17Z', ... , 'syncStatus': 'Synced'}


Execute a workflow
------------------

Upload a *hello-world* template to the git repo:

.. code-block:: yaml

  apiVersion: argoproj.io/v1alpha1
  kind: WorkflowTemplate
  metadata:
    name: hello-world
    labels:
      scenarios.ai.sap.com/id: hello-world
      ai.sap.com/version: "0.1.0"
    annotations:
      scenarios.ai.sap.com/name: hello-world
      scenarios.ai.sap.com/description : "Hello World"
      executables.ai.sap.com/name: hello-world
      executables.ai.sap.com/description: "This is a simple hello world example"
      sidecar.istio.io/inject: "false"
  spec:
    entrypoint: whalesay
    templates:
    - name: whalesay
      container:
	image: docker/whalesay:latest
	command: [cowsay]
	args: ["hello world"]

Run a workflow after the template is synced::

  >>> api.application_status("my-git")
  {'healthStatus': 'Healthy', 'message': 'successfully synced (all tasks run)', ... , 'syncStatus': 'Synced'}

  >>> hello = api.template("hello-world")
  >>> first = api.create_execution(hello)
  >>> first
  {'id': 'e4c3b115c5f781b9', 'message': 'Execution scheduled', 'status': 'UNKNOWN', 'targetStatus': 'COMPLETED'}


Monitor the flow run result
---------------------------

Check the status of the flow execution with::

  >>> api.execution(first)
  { ... 'status': 'RUNNING', ... 'targetStatus': 'COMPLETED'}

  >>> api.execution(first)
  { ... 'status': 'COMPLETED', ... 'targetStatus': 'COMPLETED'}

  >>> print(api.execution_logs(first))
   _____________ 
  < hello world >
   ------------- 
      \
       \
	\     
		      ##        .            
		## ## ##       ==            
	     ## ## ## ##      ===            
	 /""""""""""""""""___/ ===        
    ~~~ {~~ ~~~~ ~~~ ~~~~ ~~ ~ /  ===- ~~~   
	 \______ o          __/            
	  \    \        __/             
	    \____\______/   
